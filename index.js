let deviceId = '';
let config = {};

// Get the personal config, fail gracefully.
try {
  config = require('./config');
}
catch (e) {
  console.error(`
The script encountered an error related to your Pushbullet access token.
Here's how to fix it:

1. Copy file pushbullet.config.js-dummy to pushbullet.config.js
2. Get the access token from https://pushbullet.com/#settings/account
   and put it in the your new pushbullet.config.js file
3. Retry!
  `);
  process.exit(1);
}

const availableMacroKeys = Object.keys(config.kmMacros);
const deviceName = 'Keyboard Maestro';
const exec = require('child_process').exec
const process = require('process');
const PushBullet = require('pushbullet');
const pusher = new PushBullet(config.pbAccessToken);


function setUpDevice(isKeyboardMaestroKnown = false) {
  if (isKeyboardMaestroKnown) {
    console.log(`[Device#${ deviceName }] Found`);
    listenToStream();
  }
  else {
    console.log(`[Device#${ deviceName }] Creating new device`);
    pusher.createDevice(deviceName, (error, response) => {
      deviceId = response.iden;
      listenToStream();
    });
  }
}


function listenToStream() {
  const stream = pusher.stream();

  stream.connect();
  stream.on('tickle', processFreshPushes);
  processFreshPushes();
}


function processFreshPushes() {
  const options = {
    limit: 20,
  };

  pusher.history(options, (error, response) => {
    response.pushes
      .filter(push => (
        !push.dismissed
        && (
          push.target_device_iden === deviceId
          || push.title === deviceName
        )
      ))
      .forEach(processPush);
  });
}


function processPush(push = {}) {
  console.log(`[Push#${ push.iden }] Received: "${ push.body }"`);

  const parameter = normalizeString(push.body);
  const kmMacroId = availableMacroKeys.includes(parameter)
    ? config.kmMacros[parameter]
    : config.kmDefaultMacro;
  const applescript = `tell application "Keyboard Maestro Engine" to \
    do script "${ kmMacroId }" with parameter "${ parameter }"`;

  exec(`/usr/bin/osascript -e '${ applescript }'`);

  console.log(`[Push#${ push.iden }] Marking as dismissed`);
  pusher.updatePush(push.iden, { dismissed: true });
}


function normalizeString(input = '') {
  return input.replace(/[^a-z ]/g, '').replace(/\s+/g, ' ').trim();
}


function __main__() {
  console.log(`[Device#${ deviceName }] Checking for existence of device`);
  pusher.devices((error, response) => {
    let isKeyboardMaestroKnown = false;

    for (const device of response.devices) {
      if (device.nickname === deviceName) {
        isKeyboardMaestroKnown = true;
        deviceId = device.iden;
        break;
      }
    }

    setUpDevice(isKeyboardMaestroKnown);
  });
}


__main__();
